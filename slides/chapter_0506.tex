\documentclass[size=screen]{beamer}
\usepackage{beamerthemeshadow,amsmath,multimedia,graphicx,multicol}
\begin{document}
\title{Chapters 05 \& 06: Sampling \& Study Design}
\author{Instructor: John Labarga}
\date{February 23, 2021}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame} 

\section{Sampling}
\subsection{Motivation}
\begin{frame}
\frametitle{Why does sampling matter?}
\begin{itemize}
  \item To be able to extrapolate back to the population from the
    sample, the sample needs to be \emph{representative} of the
    population, meaning that it should preserve its principal
    characteristics.
  \item The most representative possible sample is a census. However,
    a census is generally impractical as previously discussed.
  \item We want to extract a subset of the population in such a way
    that we don't lose any statistical properties of the population by
    ``shrinking'' it down into a sample.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Purpose of sampling}
\begin{itemize}
  \item In addition to being significantly cheaper and easier, a
    sample is much faster to analyze than performing a census.
  \item Since a sample is a subset of the population, it cannot
    perfectly reproduce all information of the population. However, if
    the sample is correctly drawn, there are \emph{diminishing
      returns} to increasing the sample size.
  \item This means that as the sample gets larger, increasing the
    sample size further produces a falling amount of marginal benefit.
\end{itemize}
\end{frame}

\subsection{Simple Random Sampling}
\begin{frame}
\frametitle{Procedure}
\begin{itemize}
  \item The ``gold standard'' of sampling is the simplest approach:
    randomly select individual members of the population, one at a
    time until the sample size is reached.
  \item Procedure:
    \begin{itemize}
    \item Number the objects in the population
    \item Use a random number generator to select random numbers
      between 1 and $N$.
    \item If the identifier of a population member is selected, that
      member becomes a member of the sample.
    \item Repeat until sample size is reached.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Issues}
\begin{itemize}
   \item A simple random sample is not always possible.
   \item Simple random sampling assumes that you know the population
     size, which may not be the case.
   \item You may also not be able to measure every object in the
     population, which is an implicit assumption of SRS.
   \item This can cause the cost of the sampling to fluctuate
     considerably.
\end{itemize}
\end{frame}


\subsection{Stratified Sampling}
\begin{frame}
\begin{itemize}
  \item Some populations have innate sub-populations, whose
    representation in the sample is important.
  \item Examples:
    \begin{itemize}
    \item Age groups
    \item Geographic segments (countries, states, towns)
    \item Education level
    \end{itemize}
  \item Simple random sampling may produce an imbalance among these
    sub-populations, especially if the sample is relatively small.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Procedure}
\begin{itemize}
  \item Stratified sampling requires a two-stage procedure.
  \item First, identify the sizes of the sub-populations, so that the
    fraction of each sub-populations are known. Allocate portions of
    the sample according to these fractions.
  \item Randomly sample from within the sub-populations.
    \item Example: suppose you are performing stratified sampling
      based on age group. Your population is 30\% people over 65, and
      70\% people under 65. Also suppose your sample size is 100. Then
      you will sample 30 people over 65, and 70 people under 65.
\end{itemize}
\end{frame}


\subsection{Randomized Response Sampling}
\begin{frame}
\begin{itemize}
  \item It is difficult to collect data when there are anonymity or
    security concerns, e.g. when asking respondents about criminal
    activity, drug use, etc.
  \item In such contexts, a survey is more likely to get truthful
    responses if responses are correct \emph{in aggregate}, but
    individually untraceable.
  \item Study participants often do not trust that their responses
    will be kept anonymous, so it's better to design the survey to
    incorporate deniability.
  \item A common approach is to work a source of randomness into the
    response, then remove it in aggregate.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Example: measuring the prevalence of underage drinking}
\begin{itemize}
  \item High school students will usually not respond truthfully to
    questions about illegal behavior if they are not certain of the
    survey's anonymity.
  \item A common survey procedure has the respondent sit out of view
    of the administrator. The student flips a coin in a way that only
    they know whether it came up heads or tails. The student is then
    told:
    \begin{itemize}
      \item If the coin comes up heads, tell me that you have
        illegally consumed alcohol, even if you have not.
      \item If the coin comes up tails, tell me the truth.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Example: measuring the prevalence of underage drinking}
\begin{itemize}
  \item Call $P_Y$ the proportion who respond ``yes'' in this
    survey. Call $P_A$ the true proportion of high schoolers who
    drink. Then: $P_Y \approx 0.5 + 0.5 P_A$, i.e. $$P_A \approx 2(P_Y
    - 0.5)$$
  \item This approach requires more respondents to get an accurate
    measurement than a direct approach, but is totally confidential.
  \item Even if names were being recorded, each student could simply
    deny having illegally consumed alcohol by simply saying ``my coin
    came up heads''. Thus confidentiality is assured even if anonymity
    is breached.
\end{itemize}
\end{frame}

\section{Bias}
\subsection{Overview}
\begin{frame}
\begin{itemize}
  \item \emph{Bias} is a systematic difference between measurement
    results and the truth.
  \item Bias tends to be caused by an error in the sampling or survey
    procedure, leading to unreliable results.
  \item Bias is difficult to detect after the fact, and must be
    prevented ahead of time through careful survey design.      
\end{itemize}
\end{frame}

\subsection{Types of Bias}
\begin{frame}
\frametitle{Selection Bias}
\begin{itemize}
  \item Selection bias occurs when there is a relationship between the
    question of interest and whether the individual is in the sample.
    \item Examples:
    \begin{itemize}
    \item Conducting a political poll by calling random landline
      phones (older people more likely to have landline phones, and
      more likely to be conservative).
    \item Determining how long a model of car lasts by taking average
      age of examples currently registered with DMV (examples which
      have already broken down will not be registered, so this will
      inherently overestimate the longevity).
    \item Sending a satisfaction survey to rewards club members at a
      grocery store (rewards club members more likely to be long term
      customers, and more likely to be satisfied).
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Nonresponse bias}
\begin{itemize}
  \item When a survey is voluntary and requires effort on the
    respondent's part, there is a tendency for only those with extreme
    responses to return the survey.
    \item Online restaurant rating systems are a classic example. Most
      responses will be either extremely positive or extremely
      negative, since those with a regular experience will not bother
      to rate the restaurant.
    \item Online political polls are another example: only those with
      extreme opinions will tend to fill them out.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Response bias}
\begin{itemize}
  \item When a question has moral or legal implications, and anonymity
    is a concern, respondents may be less than truthful in their
    responses, or otherwise edit them.
  \item Examples: questions about drug use, criminal activity, etc.
  \item Can also occur when surveys are excessively long or detailed
    (e.g. respondents may just pick C on a multiple choice survey
    after question 20).
\end{itemize}
\end{frame}

\section{Study Design}
\subsection{Overview}
\begin{frame}
\frametitle{What is a study?}
\begin{itemize}
  \item A \emph{scientific study} is a formal investigation into an
    phenomenon using the scientific method.
  \item Statistics is involved in scientific study design not only in
    interpreting the magnitude and significance of the effect, but
    also in designing the study so that the correct effect is
    measured.
  \item Incorrectly-designed studies can have serious implications,
    like approving ineffective medicines.
  \item \emph{Note}: medical terminology will be used throughout this
    section (e.g. \emph{treatment}, \emph{patient}), but study design
    is applicable to any scientific investigation.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Two Study Types}
\begin{itemize}
  \item \emph{Observational study}: consists of reviewing historical
    information, which was produced as part of a different process
    (i.e. not for the purpose of a scientific investigation).
  \item \emph{Experiment}: a highly-controlled situation wherein the
    scientist can change the causal factor, and observe the
    outcome. The data collected is expressly for this purpose. If the
    scientist can also randomly assign levels of the causal variable
    to individuals, then this is a \emph{randomized experiment}.
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Framework of Causality}
\begin{itemize}
  \item If a change in one variable results in a repeatable,
    measurable change in another variable, the two variables are said
    to be causally related.
  \item Examples: square footage of a house and the house's price,
    temperature and energy consumption, hours slept the night before
    an exam and exam score.
  \item Causality is usually only in one direction.
  \item If two variables have a causal relationship, the ``causer'' is
    called the explanatory variable, and the ``causee'' is called the
    response variable.
\end{itemize}
\end{frame}

\subsection{Study types}
\begin{frame}
\frametitle{Observational Studies vs. Experiments}
\begin{itemize}
  \item A group of 100 students was randomly divided with 50 assigned
    to receive vitamin C and the remaining 50 to receive a placebo, to
    determine whether vitamin C helps to prevent colds.
  \item All patients who received a hip transplant operation at
    Stanford University Hospital during 1995-2005 will be followed for
    ten years after their operation to determine the success (or
    failure) of the transplant.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Observational Studies vs. Experiments}
\begin{itemize}
  \item A group of 100 students was randomly divided with 50 assigned
    to receive vitamin C and the remaining 50 to receive a placebo, to
    determine whether vitamin C helps to prevent colds.\\Answer:
    randomized experiment
  \item All patients who received a hip transplant operation at
    Stanford University Hospital during 1995-2005 will be followed for
    ten years after their operation to determine the success (or
    failure) of the transplant.\\Answer: observational study
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Observational Studies vs. Experiments}
\begin{itemize}
  \item A group of students who were enrolled in an introductory
    statistics course were randomly assigned to take a Web-based
    course or a traditional lecture course.  The two methods were
    compared by giving the same final exam in both courses.
  \item A group of smokers and a group of nonsmokers who visited a
    particular clinic were asked to come in for a physical exam every
    five years for the rest of their lives so that the researchers
    could monitor and compare their health status.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Observational Studies vs. Experiments}
\begin{itemize}
  \item A group of students who were enrolled in an introductory
    statistics course were randomly assigned to take a Web-based
    course or a traditional lecture course.  The two methods were
    compared by giving the same final exam in both courses.\\Answer:
    randomized experiment
  \item A group of smokers and a group of nonsmokers who visited a
    particular clinic were asked to come in for a physical exam every
    five years for the rest of their lives so that the researchers
    could monitor and compare their health status.\\Answer:
    observational study
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Pros and Cons}
\begin{itemize}
  \item An observational study cannot show causality, since the
    experimenter does not have control over the level of the causal
    variable.
  \item However, observational studies are much cheaper than experiments.
    \item Experiments are sometimes impossible due to ethical concerns
      (e.g. the experimenter cannot force a subject, via
      randomization, to engage in a behavior suspected to be harmful).
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Case-Control Study}
\begin{itemize}
  \item A type of observational study that more closely resembles an
    experiment. Still \textbf{unable} to prove causality.
  \item A large population of subjects with a condition (the cases)
    are found, as is a large population of subjects without the
    condition (the controls).
  \item These two populations are observed over a protracted period,
    and their outcomes are recorded.
  \item The idea is for each population to be large enough that the
    condition is the only systematic difference between them,
    strengthening the study's conclusion.
\end{itemize}
\end{frame}

\subsection{Confounding}
\begin{frame}
\frametitle{Confounding Variables}
\begin{itemize}
  \item Confounding is a major issue that can cast doubt on the
    outcome of scientific studies.
  \item Suppose the objective is to prove a causal relationship
    between A and B. A \emph{confounder} C is a variable that has a
    causal relationship with both A and B, and which was not accounted
    for in the study or data collection.
  \item When a researcher \emph{controls for} a variable, that means
    that they included a potential confounder in their analysis.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Example: Confounding}
\begin{itemize}
    \item Suppose you are studying the effect of a food X on heart
      disease, due to conventional wisdom that it is a ``healthy
      food''. You find that the incidence of heart disease is 20\%
      lower among consumers of X vs. non-consumers. It appears that X
      does have a protective effect against heart disease.
    \item On further review of the data, you notice that consumers of
      X are much more likely to be regular gym attendees than
      non-consumers of X. You know from health research that gym
      attendance protects against heart disease.
    \item After including gym attendance i your analysis, you find
      that there is actually no effect of the food X on heart
      disease. Gym attendees consume X due to its reputation as a
      health food, and their exercise is the reason they have reduced
      heart disease incidence.
\end{itemize}
\end{frame}

\subsection{Designing good experiments}
\begin{frame}
\frametitle{Randomization}
\begin{itemize}
  \item Randomization reduces the likelihood of confounding variables,
    lending more credence to the conclusions of an experiment.
  \item The core of randomization is the experimenter's ability to
    randomly assign the value or level of the causal variable, to
    observe whether this produces a repeatable effect on the response
    variable.
    \item If the experimenter cannot due this for ethical reasons,
      then an observational study is usually carried out instead.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Randomization}
\begin{itemize}
  \item If the causal variable is quantitative, the experimenter
    randomly assigns each observational unit a value (e.g. how much
    weight to lift in a study about weight lifting causing muscle
    buildup).
  \item If the causal variable is categorical, the experimenter
    randomly assigns each observational unit to a group (e.g. to
    receive aspirin or a placebo in a study of whether aspirin reduces
    tension headaches).
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Placebos}
\begin{itemize}
  \item To determine whether a treatment works, the outcome of a group
    receiving the treatment must be contrasted with a group not
    receiving the treatment.
  \item \emph{Placebo effect}: the tendency, for human subjects
    especially, to improve simply because they are receiving
    treatment, even if the treatment is ineffective.
    \item Therefore there will generally be two components to any
      improvement among patients receiving treatment: the placebo
      effect, and the actual effectiveness of the treatment (if any).
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Groups in a Placebo Study}
\begin{itemize}
  \item A study where a placebo effect is considered likely ideally
    has three groups:
    \begin{itemize}
      \item Control group: engages in the default behavior, to measure
        what would happen if a new treatment were not applied.
      \item Placebo group: receives the placebo.
      \item Treatment group: receives the treatment being studied.
    \end{itemize}
    \item The relative performance of these three groups measures the
      performance of the treatment vs. alternatives.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Outcomes of a Placebo Trial}
  C: outcome of control group

  P: outcome of placebo group

  T: outcome of treatment group
  
  \begin{center}
    \begin{tabular}{l | c | }
      Result & Implications \\
      \hline \hline
      $C \approx P \approx T$ & Drug is ineffective, no placebo effect \\ 
      $C \approx P < T$ & Drug effective, no placebo effect\\
      $C < P \approx T$ & Drug ineffective, placebo effect\\
      $C < P < T$ & Drug effective, placebo effect\\
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{What is a placebo?}

  \begin{itemize}
    \item The treatment being tested may be a new treatment competing
      with existing treatments, or it may be the first treatment for
      an illness.
    \item Additionally, the disease being treated may range from not
      serious to extremely serious.
    \item If a treatment already exists, then the placebo consists of
      giving a patient the existing treatment while telling them that
      they are receiving the new treatment. The control group gets the
      existing treatment.
      \item In some cases, using a placebo may be considered unethical
        (e.g. if there is strong evidence of efficacy before the trial
        begins).       
  \end{itemize}
  
\end{frame}

\begin{frame}
  \frametitle{Blinding}
  \begin{itemize}
    \item When using a placebo, the patient must be ``blinded'',
      i.e. they must not know whether they are receiving the real
      treatment of the placebo.
    \item However, differences in treatment may allow the patient to
      figure out whether they received the placebo or real treatment.
    \item Therefore many studies are double-blinded: neither the
      patient, nor the caregiver knows whether the patient received
      the placebo or the real treatment.
  \end{itemize}
  
\end{frame}







\end{document}
