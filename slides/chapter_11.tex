\documentclass[size=screen,xcolor=table]{beamer}
\usepackage{beamerthemeshadow,amsmath,multimedia,graphicx,multicol,multirow,mathtools}
\begin{document}
\title{Chapter 11: Confidence Intervals for the Population Mean}
\author{Instructor: John Labarga}
\date{April 20, 2021}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame} 

\section{Single Sample Case}

\subsection{Constructing a Confidence Interval for $\mu$}

\begin{frame}

  The overall logic of confidence intervals is the same, whether we
  use them to estimate $p$ or $\mu$.

  \vspace{3mm}

  Out confidence intervals for $\mu$ have the same basic components:

  $$\left[ \mbox{point estimate} \pm \mbox{multiplier} \cdot
    \mbox{standard error}\right]$$

  \vspace{3mm}

  The main distinctions in the case of estimating the $\mu$ of a
  continuous distribution with a confidence interval, is whether the
  data is known to be coming from a normal distribution, and whether
  $\sigma$ is known. 

\end{frame}


\begin{frame}
  \frametitle{Case of Known $\sigma$}

  In the simplest case, where we know that the population is normally
  distributed with a known $\sigma$, the sampling distribution of
  $\bar{x}$ is:

  $$N \left( \mu, \frac{\sigma}{\sqrt{n}} \right)$$

  This yields a simple confidence interval for $\mu$:

  $$\left[ \mu - z \frac{\sigma}{\sqrt{n}}, \mu + z
    \frac{\sigma}{\sqrt{n}} \right]$$

  where: $$z = \left| \mbox{invNorm}\left(\frac{1 - \mbox{CL}}{2}\right) \right|$$

\end{frame}


\begin{frame}
  \frametitle{Relaxing Our Assumptions}

  Generally we don't know the value of $\sigma$, and often we
  additionally suspect (e.g. from a histogram or boxplot) that the
  population distribution is not normal.

  \vspace{3mm}

  In this case, we have to substitute the standard error:

  $$\frac{s}{\sqrt{n}}$$

  where $s$ is the sample standard deviation. However since $s$ is a
  random variable, we need to change our multiplier to compensate for
  the fact that it will vary (unlike a fixed value $\sigma$).

  \vspace{3mm}
  
  We do this by substituting our $z$ multiplier for a multiplier from
  the $t$ distribution.

\end{frame}

\begin{frame}
  \frametitle{The $t$ Distribution}

  \begin{itemize}

  \item Family of distribution curves similar in shape to the standard
    normal distribution, but with wider and longer tails

  \item Centered at 0, and symmetric

  \item Has one parameter, called the ``degrees of freedom'', equal to
    $n-1$

  \item As $n$ gets larger, the $t$ distribution eventually converges
    to the standard normal distribution

  \end{itemize}

  \vspace{3mm}

  The $t$ multiplier we'll use in our confidence intervals is a
  percentile of this $t$ distribution.

\end{frame}

\begin{frame}
  \frametitle{$t$ Intervals}

  The $t$ interval is appropriate in either of these two situations:

  \begin{enumerate}

    \item A sample of any size is drawn from a normal distribution,
      but the $\sigma$ of that normal distribution is unknown.

    \item A sample of at least size 30 is drawn from a non-normal
      distribution.

  \end{enumerate}

  If the conditions are met for using a $t$ interval, our confidence
  interval is:

  $$\left[ \bar{x} - t \frac{s}{\sqrt{n}}, \bar{x} + t
    \frac{s}{\sqrt{n}} \right]$$

  where $t$ is the $\frac{1 - \mbox{CL}}{2}$ percentile of a $t$
  distribution with degrees of freedom equal to $n-1$.

\end{frame}

\subsection{Examples}

\begin{frame}
  \frametitle{Example: Estimating Hours of TV Watched Per Day}

  Suppose a researcher wants to estimate the mean hours of TV watched
  per day by college students. The researcher surveys 175 college students,
  who report watching an average of 2.09 hours of TV per day, with a
  standard deviation of 1.644 hours. The histogram of the data reveals
  a strong positive skew.

  \vspace{3mm}

  Build a 99\% confidence interval for $\mu$, the mean amount of TV
  watched by college students per day.

  \vspace{27mm}

\end{frame}

\begin{frame}
  \frametitle{Example: Estimating Hours of TV Watched Per Day}

  Suppose a researcher wants to estimate the mean hours of TV watched
  per day by college students. The researcher surveys 175 college students,
  who report watching an average of 2.09 hours of TV per day, with a
  standard deviation of 1.644 hours. The histogram of the data reveals
  a strong positive skew.

  \vspace{3mm}

  Build a 99\% confidence interval for $\mu$, the mean amount of TV
  watched by college students per day.

  \vspace{3mm}

  $$\mbox{STAT} \rightarrow \mbox{Tests} \rightarrow
  \mbox{TInterval}$$

  $$[1.7663, 2.4137]$$

\end{frame}

\begin{frame}
  \frametitle{Example: Spending on School Supplies}

  Twelve students were asked how much they spent on books this
  semester, with the goal of estimating the mean amount spent on books
  by all students at the university. The twelve responses were:

  $$[500, 450, 475, 500, 750, 620, 600, 670, 650, 700, 550, 550]$$

  \vspace{3mm}

  Build a 95\% confidence interval for $\mu$, the mean amount spent by
  students at the university on books.

\end{frame}

\section{Two Sample Case}

\begin{frame}
  \frametitle{Differences Between the $p$ and $\mu$ Two-Sample Cases}

  In the two-sample case for the population proportion $p$, we assumed
  that the two samples were drawn from independent, unrelated
  populations.

  \vspace{5mm}

  In the two-sample case for $\mu$, we will allow for potential
  dependence between pairs of observations across the two
  samples. This is called \emph{paired data}.

  \vspace{5mm}

  As with $p$, we will also examine the case where the two samples are
  from independent populations.

\end{frame}

\subsection{Paired Differences}

\begin{frame}
  \frametitle{Sources of Paired Data}

  \emph{Paired} numerical data arises when two populations are
  structured such that each entry in population 1 has a corresponding
  entry in population 2. Sampling from these populations similarly
  produces datapoints that are ``paired up''.

  \vspace{3mm}

  Examples:

  \begin{itemize}

    \item A person is given some training to improve at a skill. Their
      ability is tested once before the training, then again
      after. The before and after scores form the pairs.

    \item The tire pressure is measured on the front tires of 1000
      cars. The pairs of left and right air pressure readings form the
      pairs. 

  \end{itemize}
  

\end{frame}

\begin{frame}
  \frametitle{Confidence Intervals for Paired Data}

  Instead of treating the two pairs as separate datasets, we'll
  consider the \emph{difference between them} to be the variable whose
  mean, $\mu_d$, we're trying to build a confidence interval for:

  \begin{table}[]
    \begin{tabular}{|c|c|c|}
      \hline
      X   & Y   & Difference ($d$) \\ \hline
      A   & B   & A-B        \\ \hline
      ... & ... & ...        \\ \hline
    \end{tabular}
  \end{table}

   We can treat this difference variable, $d$, as the variable of interest,
   and calculate is mean ($\bar{d}$) and its standard deviation
   ($s_d$).

   \vspace{3mm}

   If all of the criteria for using a $t$-interval are met, we can use
   a $t$-interval to construct a confidence interval for $\mu_d$ of
   the form:

   $$\left[ \bar{d} - t \frac{s_d}{\sqrt{n}}, \bar{d} + t \frac{s_d}{\sqrt{n}} \right]$$

 \end{frame}

\begin{frame}
  \frametitle{Example: Does Meditation Reduce Blood Pressure?}

  To determine if meditation reduces blood pressure, fifteen
  volunteers will have their blood pressure measured before and after
  meditation.

  From the following data, build a 95\% confidence interval for
  $\mu_s$, the mean change in blood pressure after meditation.

  \begin{table}[]
    \begin{tabular}{|c|c|c|c|c|c|}
      \hline
      Before & After & Before & After & Before & After \\ \hline
      165    & 162   & 138    & 130   & 145    & 145   \\ \hline
      156    & 150   & 155    & 148   & 143    & 137   \\ \hline
      141    & 132   & 180    & 170   & 137    & 121   \\ \hline
      171    & 172   & 152    & 140   & 139    & 136   \\ \hline
      140    & 138   & 166    & 161   & 141    & 141   \\ \hline
    \end{tabular}
  \end{table}

\end{frame}

\subsection{Independent Populations}

\begin{frame}
  \frametitle{Unpaired Data from Unrelated Populations}

  Paired data is a special case. In general we will be sampling from
  two independent populations, producing samples with no link between
  the observations (the samples will generally not even be the same
  size). 

  \vspace{3mm}

  Examples:

  \begin{itemize}

    \item Final exam grades in two different classes of students.

    \item Blood pressure readings from a control group and a group
      assigned to take aspirin once a day.

    \item Ages of residents in two different towns.

  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Notation}

  We are drawing two samples from two populations:

  \begin{table}[]
    \begin{tabular}{|c|c|c|}
      \hline
      & Population 1 & Population 2 \\ \hline
      Population Mean               & $\mu_1$      & $\mu_2$      \\ \hline
      Population Standard Deviation & $\sigma_1$   & $\sigma_2$   \\ \hline
      Sample Size                   & $n_1$        & $n_2$        \\ \hline
      Sample Mean                   & $\bar{x}_1$  & $\bar{x}_2$  \\ \hline
      Sample Standard Deviation     & $s_1$        & $s_2$        \\ \hline
    \end{tabular}
  \end{table}

  The quantity we're building a confidence interval for is $\mu_1 -
  \mu_2$.

  \vspace{3mm}

  \underline{Pooling}: if it's suspected that $\sigma_1 = \sigma_2$
  (i.e. the two populations are thought to have different means but
  the same standard deviation), a \emph{pooled} estimate of the
  standard error can be used. In this course we will not make this
  assumption, and will use an \emph{unpooled} estimate of the standard
  error.

\end{frame}

\begin{frame}
  \frametitle{Two=-Sample $t$ Confidence Interval}

  To build a confidence interval for $\mu_1 - \mu_2$, we will use a
  two-sample version of the $t$-interval used for the one-sample
  case.

  \vspace{3mm}

  To do this, we need the criteria for the $t$-interval to be met for
  \emph{each} of the samples, i.e. either:

  \begin{itemize}

  \item Both of the samples come from a normal distribution, or

  \item Both samples come from an arbitrary distribution, but each
    sample has a sample size of at least 30.

  \end{itemize}

  If one of these conditions is met, the appropriate function on the
  calculator is:

  $$\mbox{STAT} \rightarrow \mbox{Tests} \rightarrow
  \mbox{2-SampTInterval}$$

\end{frame}

\begin{frame}
  \frametitle{Example: Does Exercise Reduce Resting Heart Rate?}

  The resting heart rate is measured on volunteers from two groups:
  one group exercises regularly, the other does not.

  \vspace{3mm}

  The resting heart rates for the group of exercisers:

  $$[62, 72, 60, 63, 75, 64, 60, 52, 64, 80, 68, 64]$$

  \vspace{3mm}

  The resting heart rates for the group of exercisers:

  $$[72, 84, 66, 72, 62, 84, 76, 60]$$

  \vspace{3mm}

  Build a 95\% confidence interval for the difference between their
  mean resting heart rates, and conclude whether exercise reduces
  resting heart rate on average.

  
\end{frame}


\end{document}
